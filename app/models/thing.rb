class Thing < ApplicationRecord
  serialize :image_meta_data

  has_many :authorships
  has_many :users, through: :authorships

  has_many :comments, dependent: :destroy

  scope :latest, -> { all.limit(9).order(id: :desc) }
end
